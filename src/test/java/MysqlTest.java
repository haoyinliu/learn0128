import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.sql.DataSource;
import java.sql.Connection;

@SpringBootTest
public class MysqlTest {

    @Autowired
    DataSource dataSource;

    @Test
    public void test1() throws Exception{
        System.out.println(dataSource.getClass());
        Connection connection = dataSource.getConnection();
        //Connection connection = dataSource.getConnection();
        System.out.println(connection);

        //template模板，拿来即用
        connection.close();

    }
}
