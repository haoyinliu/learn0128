package com.learn.leetcode;

import java.util.Stack;

public class Lesson844Bug {
    public static void main(String[] args) {


    }

    public void fun1(){
        String s = "huzkhfgkldzf";
        String k = "huzkhfgkldzf";

    }

    public boolean backspaceCompare(String s, String t){
        Stack<Character> stackS = changeStringToStack(s);
        Stack<Character> stackK = changeStringToStack(t);
        return compareTwoStackEqual(stackS,stackK);
    }

    public Stack<Character> changeStringToStack(String s){

        Stack<Character> stack = new Stack<Character>();
        for(char c:s.toCharArray()){
            if(c=='#' && stack.size()>0){//这是一个Bug,stack.size()=0的时候，会把#放进去
                stack.pop();
            }else{
                stack.push(c);
            }
        }
        return stack;
    }

    public boolean compareTwoStackEqual(Stack<Character> s,Stack<Character> t){

        if(s.size()!=t.size()){
            return false;
        }else{
            while(s.size()>0){
                if(!(s.pop().equals(t.pop()))){
                    return false;
                }
            }
        }
        return true;
    }

}
