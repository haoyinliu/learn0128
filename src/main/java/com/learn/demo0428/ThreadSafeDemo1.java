package com.learn.demo0428;

import org.junit.Test;

import java.util.HashMap;
import java.util.concurrent.atomic.AtomicInteger;

public class ThreadSafeDemo1 {

    @Test
    public void test1(){

        AtomicInteger atomicInteger = new AtomicInteger();
        System.out.println(atomicInteger.get());
        for(int i=1;i<10;i++){
            System.out.println(atomicInteger.incrementAndGet());
        }
    }

    @Test
    public void test(){
        HashMapThread thread = new HashMapThread();
        thread.start();
    }

    public static void main(String[] args) {
        HashMapThread thread = new HashMapThread();
        thread.start();
        HashMapThread thread1 = new HashMapThread();
        thread.start();
        HashMapThread thread2 = new HashMapThread();
        thread.start();
    }
}

class HashMapThread extends Thread{

    private static AtomicInteger atomicInteger = new AtomicInteger();
    private static HashMap map = new HashMap();

    @Override
    public void run() {
        while(atomicInteger.get()<100){
            map.put(atomicInteger.get(), atomicInteger.get());
            atomicInteger.incrementAndGet();
            System.out.println(atomicInteger.get());
        }

    }
}