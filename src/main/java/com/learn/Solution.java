package com.learn;

class Solution {
    public boolean backspaceCompare(String s, String t) {
        //方法1
        // return build(s).equals(build(t));

        //方法2
        int i = s.length() - 1, j = t.length() - 1; //尾指针
        int sSkip = 0, tSkip = 0;
        while(i >= 0 || j >= 0) {
            //遍历字符串s
            while(i >= 0) {
                if(s.charAt(i) == '#') {
                    sSkip++;
                    i--;
                } else if(sSkip > 0) {
                    sSkip--;
                    i--;
                } else {
                    break;
                }
            }

            //遍历字符串t
            while(j >= 0) {
                if(t.charAt(j) == '#') {
                    tSkip++;
                    j--;
                } else if(tSkip > 0) {
                    tSkip--;
                    j--;
                } else {
                    break;
                }
            }

            //比较有效的字符
            if(i >= 0 && j >= 0) {
                if(s.charAt(i) != t.charAt(j)) {
                    return false;
                }
            } else {
                if(i >= 0 || j >= 0) {
                    return false;
                }
            }

            i--;
            j--;
        }
        return true;
    }

    // public String build(String str) {
    //     StringBuffer temp = new StringBuffer();
    //     int length = str.length();
    //     for(int i = 0; i < length; i++) {
    //         char ch = str.charAt(i);
    //         if(ch != '#') {
    //             temp.append(ch);
    //         } else {
    //             if(temp.length() > 0) {
    //                 temp.deleteCharAt(temp.length() - 1);
    //             }
    //         }
    //     }
    //     return temp.toString();
    // }
}