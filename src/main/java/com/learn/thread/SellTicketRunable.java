package com.learn.thread;

public class SellTicketRunable implements Runnable{

    String theadName;

    public SellTicketRunable(String theadName) {
        this.theadName = theadName;
    }

    @Override
    public void run() {
        for(int i=40;i>0;i--){
            System.out.println(theadName+"--"+i);
            try {
                Thread.sleep(50);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void start(){
        Thread thread = new Thread(this, theadName);
        thread.start();
    }

    public static void main(String[] args) {
        SellTicketRunable t1 = new SellTicketRunable("A");
        t1.start();
        SellTicketRunable t2 = new SellTicketRunable("B");
        t2.start();
    }
}
