package com.learn;

import com.google.gson.Gson;
import java.util.HashMap;
import java.util.Map;

public class BuildToken {

    public static void main(String[] args) {

        String openApiUrl = "https://open-api-dbox.yyuap.com/open-auth/selfAppAuth/getAccessToken";
        String token = null;
        Map<String, Object> params = new HashMap<String, Object>();
        // 除签名外的其他参数
        params.put("appKey", "2eb4f91fbfc146d2be9e428a8f89a154");
        params.put("timestamp", String.valueOf(System.currentTimeMillis()));
        // 计算签名

        String signature = null;
        String responseString = null;
        try {
            signature = SignHelper.sign(params, "68ace17fcf4749ce9d8c7ee31bee6b68");
            params.put("signature", signature);
            responseString = RequestTool.doGet(openApiUrl, params);
        }catch (Exception e){
            e.printStackTrace();
        }
        Gson gson = new Gson();
        Map result = gson.fromJson(responseString,Map.class);
        if(ResultCode.SUCCESS.getIndex().equals(result.get(ResultCode.SUCCESS.getName()))) {
            Map<String, Object> tokenInfo = (Map<String, Object>) result.get("data");
            String access_token = (String) tokenInfo.get("access_token");
            String[] expireStr = String.valueOf(tokenInfo.get("expire")).split("\\.");
            Long expire = Long.valueOf(expireStr[0]);
            token = access_token;
            System.out.println(token);
        }

    }

}
