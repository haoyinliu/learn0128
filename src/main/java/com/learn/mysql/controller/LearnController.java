package com.learn.mysql.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/test")
public class LearnController {

    @RequestMapping("/hello")
    public String test(){
        return "hello";
    }


}
