package com.learn.mysql.controller;

import com.learn.mysql.pojo.Certication;
import com.learn.mysql.service.CerticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;

/**
 * @Author: Yinliu Hao
 * @Description:
 * @Date: Created in 2022/4/18 14:13
 * @Modify By:
 */
@RestController
@RequestMapping("/cert")
public class CerticationController {

    @Autowired
    CerticationService certicationService;

    @RequestMapping("/list")
    public List<Certication> queryUserAllCertications() throws Exception {
        Certication cert = new Certication();
        List<Certication> certications = certicationService.getallCertByMap(cert);
        return certications;
    }


}
