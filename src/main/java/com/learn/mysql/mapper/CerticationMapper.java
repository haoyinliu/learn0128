package com.learn.mysql.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;
import com.learn.mysql.pojo.Certication;
import java.util.List;

@Mapper
@Repository
public interface CerticationMapper {

    List<Certication> queryCertsByMap(Certication param);
}