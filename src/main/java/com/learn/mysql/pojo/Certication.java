package com.learn.mysql.pojo;


import java.io.Serializable;
import java.util.Date;

/**
 * project name: ym-exam-api
 * Date: 2022/4/15 17:13
 *
 * @PackgeName: com.yonyou.exam.modules.certication.entity
 * @Author: GXG
 * @Version：1.00
 * @Description:
 */
public class Certication implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    private String id;

    /**
     * 等级
     */
    private Integer quLevel;

    /**
     * 方向
     */
    private Integer quOri;

    /**
     * 组卷规则配置id
     */
    private Integer examRuleId;

    /**
     * 友户通ID
     */
    private String userId;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 截至时间
     */
    private Date endTime;

    /**
     * 更新时间
     */
    private Date modifyTime;

    /**
     * 资格状态（0:未通过,1：已通过,2:已过期,3：进行中）
     */
    private Integer status;

    /**
     * 类型（0：线上答题，1：应用开发，2：答辩）
     */
    private Integer type;

    /**
     * 剩余考试次数
     */
    private Integer numberRemain;

    /**
     * 最新分数
     */
    private Integer lastScore;

    /**
     * 试卷id
     */
    private String paperId;


    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getQuLevel() {
        return quLevel;
    }

    public void setQuLevel(Integer quLevel) {
        this.quLevel = quLevel;
    }

    public Integer getQuOri() {
        return quOri;
    }

    public void setQuOri(Integer quOri) {
        this.quOri = quOri;
    }

    public Integer getExamRuleId() {
        return examRuleId;
    }

    public void setExamRuleId(Integer examRuleId) {
        this.examRuleId = examRuleId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Date getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getNumberRemain() {
        return numberRemain;
    }

    public void setNumberRemain(Integer numberRemain) {
        this.numberRemain = numberRemain;
    }

    public Integer getLastScore() {
        return lastScore;
    }

    public void setLastScore(Integer lastScore) {
        this.lastScore = lastScore;
    }

    public String getPaperId() {
        return paperId;
    }

    public void setPaperId(String paperId) {
        this.paperId = paperId;
    }

    @Override
    /**
     * toString
     */
    public String toString() {
        return "Certication {" +
                "id = " + id  + ", " +
                "quLevel = " + quLevel  + ", " +
                "quOri = " + quOri  + ", " +
                "examRuleId = " + examRuleId  + ", " +
                "userId = " + userId  + ", " +
                "createTime = " + createTime  + ", " +
                "endTime = " + endTime  + ", " +
                "modifyTime = " + modifyTime  + ", " +
                "status = " + status  + ", " +
                "type = " + type  + ", " +
                "numberRemain = " + numberRemain  + ", " +
                "lastScore = " + lastScore  + ", " +
                "paperId = " + paperId ;
    }
}