package com.learn.mysql.service.impl;

import com.learn.mysql.mapper.CerticationMapper;
import com.learn.mysql.pojo.Certication;
import com.learn.mysql.service.CerticationService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * @Author: Yinliu Hao
 * @Description:
 * @Date: Created in 2022/4/18 12:03
 * @Modify By:
 */
@Service
@Slf4j
public class CerticationServiceImpl implements CerticationService {

    @Autowired
    CerticationMapper certicationMapper;



    @Override
    public List<Certication> getallCertByMap(Certication param) throws Exception {
        List<Certication> resultList = certicationMapper.queryCertsByMap(param);

        return resultList;
    }

    public void validateParams(Certication certication) throws Exception {
        if (null == certication.getQuLevel()) {
            throw new Exception("缺少必填项quLevel");
        }
        if (null == certication.getQuOri()) {
            throw new Exception("缺少必填项quOri");
        }
        if (null == certication.getExamRuleId()) {
            throw new Exception("缺少必填项examRuleId");
        }
        if(null==certication.getUserId()){
            throw new Exception("缺少必填项userId");
        }
        if(null==certication.getStatus()){
            throw new Exception("缺少必填项status");
        }
        if(null==certication.getType()){
            throw new Exception("缺少必填项type");
        }

    }


}

