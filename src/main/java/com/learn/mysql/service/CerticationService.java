package com.learn.mysql.service;


import java.util.List;
import com.learn.mysql.pojo.Certication;


/**
 * @Author: Yinliu Hao
 * @Description:
 * @Date: Created in 2022/4/18 12:02
 * @Modify By:
 */
public interface CerticationService {


    List<Certication> getallCertByMap(Certication param) throws Exception;

}
