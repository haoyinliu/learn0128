package com.learn.privateNew;

/**
 * @Author: Yinliu Hao
 * @Description:
 * @Date: Created in 2022/1/30 20:49
 * @Modify By:
 */
public class Student {
    private int age;

    public Student(int age) {
        this.age = age;
    }
}
