package com.learn;

import com.learn.leetcode.Lesson844Bug;
import com.learn.test.Student;

import java.util.HashMap;
import java.util.Stack;

/**
 * @Author: Yinliu Hao
 * @Description:
 * @Date: Created in 2022/2/8 20:18
 * @Modify By:
 */
public class Test {
    public static void main(String[] args) {
        HashMap<String,Object> map = new HashMap<>();
    }

    @org.junit.Test
    public void fun1(){

        String s = "y#fo##f";
        String k = "y#f#o##f";
        Lesson844Bug lesson844 = new Lesson844Bug();
        System.out.println(lesson844.backspaceCompare(s,k));
    }

    @org.junit.Test
    public void fun2(){
        Stack<Student> stackS = new Stack<Student>();
        stackS.peek();
    }

    @org.junit.Test
    public void fun3(){
        byte[] bs = {(byte)0xff, 0x0F, 0x1F, 0x2F, 0x3F, 0x4F, 0x5F, 0x6F};
        for(int i=0;i<bs.length;i++){
            int tempI=(int)(bs[i] & 0xff);
            String temp16=Integer.toHexString(tempI);
            System.out.println("i="+i+", 10="+tempI +", 16="+temp16);
        }
    }

    @org.junit.Test
    public void fun4(){
        System.out.println(Integer.toBinaryString(127));
        System.out.println(Integer.toBinaryString(-127));
        System.out.println(Integer.toBinaryString(-128));

    }


    @org.junit.Test
    public void fun5(){
        float f1= 3.48645610000554f;
        System.out.println(f1);

        byte b1 = 0x20;
        System.out.println(b1);

    }

    @org.junit.Test
    public void fun6(){
        System.out.println(Float.floatToIntBits(454654.456131f));

    }

    @org.junit.Test
    public void fun7(){
        Integer a = 2000;
        Integer b = 2000;
        System.out.println(a.equals(b));
        System.out.println(a==b);
        int c =2000;
        int d = 2000;
        System.out.println(c==d);
    }





}
