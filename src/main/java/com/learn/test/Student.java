package com.learn.test;

import com.alibaba.fastjson.JSONObject;

import java.util.Date;

public class Student {
    private String name;
    private int age;
    private Date birthday;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public static void main(String[] args) {
        Student student = new Student();
        student.age = 15;
        student.name = "najek";
        student.birthday = new Date();
        String s = JSONObject.toJSONString(student);
        System.out.println(s);
        JSONObject res = JSONObject.parseObject(s);
        System.out.println(res);
    }
}
