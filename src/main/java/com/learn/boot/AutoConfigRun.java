package com.learn.boot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author: Yinliu Hao
 * @Description:
 * @Date: Created in 2022/1/28 0:51
 * @Modify By:
 */
@EnableAutoConfiguration
@RestController
public class AutoConfigRun {
    public static void main(String[] args) {
        SpringApplication.run(AutoConfigRun.class, args);
    }

    @GetMapping("/test")
    public String test(){
        return "test";
    }
}
