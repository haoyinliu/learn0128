package com.learn.boot;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author: Yinliu Hao
 * @Description:
 * @Date: Created in 2022/1/28 0:58
 * @Modify By:
 */
@RestController
public class TestController {

    @GetMapping("demo")
    public String demo(){
        return "demo";
    }
}
