package com.learn.boot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Author: Yinliu Hao
 * @Description:
 * @Date: Created in 2022/1/28 0:56
 * @Modify By:
 */
//@SpringBootApplication
public class SpringBootApplicationRun {
    public static void main(String[] args) {
        SpringApplication.run(SpringBootApplicationRun.class, args);
    }
}
