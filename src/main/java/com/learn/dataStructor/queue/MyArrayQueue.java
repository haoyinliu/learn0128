package com.learn.dataStructor.queue;

import java.util.Stack;

public class MyArrayQueue {

    Object[] elements;
    int size;
    int head;
    int tail;

    public static void main(String[] args) {
        fun2();
        Stack<Integer> stack = new Stack<Integer>();
        stack.size();
    }

    public static void fun1(){
        MyArrayQueue queue = new MyArrayQueue(10);
        queue.enQuene("1");
        queue.enQuene("2");
        queue.enQuene("3");
        queue.enQuene("4");
        queue.enQuene("5");
        queue.enQuene("6");
        queue.enQuene("7");

        Object result = queue.deQuene();
        System.out.println(result);
        Object result1 = queue.deQuene();
        System.out.println(result1);
        queue.deQuene();
        System.out.println(queue.head);
        System.out.println(queue.tail);
    }

    public static void fun2(){
        MyArrayQueue queue = new MyArrayQueue(5);
        for(int i=0;i<10;i++){
            System.out.println(queue.enQuene(i));
        }
    }

    public MyArrayQueue(int n){
        elements = new Object[n];
        this.size = n;
    }

    public boolean enQuene(Object o){
        if(size==tail){
            return false;//也可以扩容
        }
        elements[tail++] = o;
        return true;
    }

    public Object deQuene(){
        if(head==tail){
            return null;
        }
        return elements[head++];
    }

}
