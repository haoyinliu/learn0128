package com.learn.dataStructor.array;

/**
 * @Author: Yinliu Hao
 * @Description:
 * @Date: Created in 2022/2/8 20:20
 * @Modify By:
 */
public class ArrayTest {
    public static void main(String[] args) throws InterruptedException {
        ArrayLearn arrayLearn= new ArrayLearn();
        arrayLearn.add(9);
        arrayLearn.add(8);
        arrayLearn.add(7);
        System.out.println("index"+arrayLearn.size());
        arrayLearn.print();
        Thread.sleep(10000);
    }
}
