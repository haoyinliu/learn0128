package com.learn.dataStructor.array;

/**
 * @Author: Yinliu Hao
 * @Description:
 * @Date: Created in 2022/2/8 19:12
 * @Modify By:
 */
public class ArrayLearn {

    private static int[] elements = new int[100];
    private static int index;//标识数组中当前坐标位置

    public static void main(String[] args) throws Exception {

        fun1();
    }

    public void add(int i){
        elements[index++] = i;
    }

    public void insert(int i,int element) throws Exception {
        if(i>index){
            throw new Exception("越界");
        }
        //从末尾开始一个一个挪
        for(int j=index;j>i;j--){
            elements[j] = elements[j-1];
        }
        elements[i] = element;
        index++;//坐标记得加一
    }

/*    public void addArray(Array array){
        for(int i=0;i<Array.getLength(array);i++){
            add(array[i]);
        }
    }*/

    public void delete(int i) throws Exception {
        for(int j=0;j<index;j++){
            if(elements[j] == i){
                deleteByIndex(j);
            }
        }
    }

    public void deleteByIndex(int delIndex) throws Exception {
        if(delIndex >=index){
            throw new Exception("越界");
        }
        for(int i=delIndex;i<index;i++){
            elements[i] = elements[i+1];
        }
        index--;
    }

    public int get(int i) throws Exception {
        for(int j=0;j<index;j++){
            if(elements[j]==i){
                return getByIndex(j);
            }
        }
        return -1;
    }

    public int getByIndex(int getIndex) throws Exception {

        if(getIndex>=index){
            throw new Exception("越界");
        }
        return elements[getIndex];
    }


    public int size(){
        return index;
    }

    public void print(){
        for(int i=0;i<index;i++){
            System.out.println(elements[i]);
        }
    }

    public static void fun1() throws Exception {
        ArrayLearn arrayLearn= new ArrayLearn();
        arrayLearn.add(9);
        arrayLearn.add(8);
        arrayLearn.add(7);
        arrayLearn.add(6);
        arrayLearn.add(5);
        arrayLearn.add(4);
        arrayLearn.add(3);
        arrayLearn.add(2);
        arrayLearn.add(1);
        int res = arrayLearn.getByIndex(5);
        System.out.println("res+===="+res);
    }






}
