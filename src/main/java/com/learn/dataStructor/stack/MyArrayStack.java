package com.learn.dataStructor.stack;

import java.util.Arrays;

public class MyArrayStack {

    private static final int DEFAULT_CAPACITY = 10;

    Object[] elements;
    int count;
    int size;

    public MyArrayStack(){
        elements = new Object[DEFAULT_CAPACITY];
        size = DEFAULT_CAPACITY;
        count = 0;
    }

    public MyArrayStack(int n){
        elements = new Object[n];
        size = n;
        count = 0;
    }

    public boolean push(Object o){
        if(count==size){
           growCapacity();
        }
        elements[count++] = o;
        return true;
    }

    public Object pop(){
        if(count ==0){
            return null;
        }else{
            return elements[--count];
        }
    }

    public Object top(){
        if(count ==0){
            return null;
        }else{
            return elements[count];
        }
    }

    public Object getMin(){
        int temp = count;
        int min =(Integer)top();
        while(temp>-1){
            if((Integer)elements[temp-2]<min){
                min = (Integer)elements[temp-2];
                temp--;
            }else{
                temp--;
            }
        }
        return min;
    }

    public boolean growCapacity() {
        size = elements.length+DEFAULT_CAPACITY;
        elements= Arrays.copyOf(elements, size);
        return true;
    }

    public static void main(String[] args) {
        MyArrayStack stack = new MyArrayStack(5);
        stack.push(1);
        stack.push(2);
        stack.push(3);
        stack.push(4);
        stack.push(5);
        stack.push(6);
        stack.push(7);
        System.out.println(stack.size);
        System.out.println(stack.count);
        System.out.println(stack.getMin());
    }

}
