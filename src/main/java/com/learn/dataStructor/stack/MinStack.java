package com.learn.dataStructor.stack;

import java.util.Arrays;
import java.util.Stack;

/**
 * @Author: Yinliu Hao
 * @Description:
 * @Date: Created in 2022/2/19 15:43
 * @Modify By:
 */
public class MinStack {

    private static final int DEFAULT_CAPACITY = 10;

    int[] elements;
    int count;
    int size;

    public MinStack() {
        elements = new int[DEFAULT_CAPACITY];
        size = DEFAULT_CAPACITY;
        count = 0;
    }

    private boolean growCapacity() {
        size = elements.length+DEFAULT_CAPACITY;
        elements= Arrays.copyOf(elements, size);
        return true;
    }

    public void push(int val){
        if(count==size){
            growCapacity();
        }
        elements[count++] = val;
    }

    public void pop() {
        if(count >0){
            --count;
        }
    }

    public int top() {

        return elements[count-1];

    }

    public int getMin() {
        int temp = count;
        int min =top();
        while(temp>1){
            if(elements[temp-2]<min){
                min = elements[temp-2];
                temp--;
            }else{
                temp--;
            }
        }
        return min;
    }

    public static void main(String[] args) {
        MinStack minStack = new MinStack();
        minStack.push(-2);
        minStack.push(-0);
        minStack.push(-3);
        System.out.println(minStack.getMin());
        minStack.pop();
        System.out.println(minStack.top());
        System.out.println(minStack.getMin());
    }
}
