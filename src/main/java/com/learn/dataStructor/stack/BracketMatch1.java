package com.learn.dataStructor.stack;

import java.util.Arrays;

//自己写的，只适用于()[]{}这种依次类型，不适用于{[{}]}这种嵌套类型
public class BracketMatch1 {
    public static void main(String[] args) {
        System.out.println(isValid("()[]{}(}"));
    }

    private static boolean isValid(String s) {

        char[] allLeft = new char[]{'(','[','{'};
        char[] allRight = new char[]{')',']','}'};
        MyArrayStack leftStack = new MyArrayStack();
        MyArrayStack rightStack = new MyArrayStack();


        char[] elements = s.toCharArray();
        for(int i=0;i<elements.length;i++){
            if(Arrays.binarySearch(allLeft, elements[i])>-1){
                leftStack.push(elements[i]);
            }else{
                rightStack.push(elements[i]);
            }
        }

        for(int i=0;i<elements.length/2;i++){
            if(!(match((char)leftStack.pop(), (char)rightStack.pop()))){
                return false;
            }
        }
        if(leftStack.count>0 || rightStack.count >0){
            return false;
        }

        return true;
    }

    private static boolean match(char a,char b){
        if('('==a && ')'==b){
            return true;
        }
        if('['==a && ']'==b){
            return true;
        }
        if('{'==a && '}'==b){
            return true;
        }
        return false;
    }
}
