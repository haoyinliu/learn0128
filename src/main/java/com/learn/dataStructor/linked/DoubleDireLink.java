package com.learn.dataStructor.linked;


public class DoubleDireLink {


    protected DoubleDireNode root;
    protected int count;


    public static void main(String[] args) {
        DoubleDireLink link = new DoubleDireLink();
        DoubleDireNode node1 = new DoubleDireNode("1");
        DoubleDireNode node2 = new DoubleDireNode("2");
        DoubleDireNode node3 = new DoubleDireNode("3");
        DoubleDireNode node4 = new DoubleDireNode("4");
        DoubleDireNode node5 = new DoubleDireNode("5");
        DoubleDireNode node6 = new DoubleDireNode("6");
        DoubleDireNode node7 = new DoubleDireNode("7");
        link.add(node1);
        link.add(node2);
        link.add(node3);
        link.add(node4);
        link.add(node5);
        link.add(node6);
        link.add(node7);
        //link.print();
        link.removeRoot();
        System.out.println(link.root.data);
    }

    public DoubleDireLink(){

    }

    public DoubleDireLink(DoubleDireNode root){
        this.root = root;
        root.next = root;
        root.pre = root;
        this.count = 1;
    }

    //初始化约瑟夫环专用
    public DoubleDireLink(int count){
        for(int i=1;i<=count;i++){
            add(new DoubleDireNode(i));
        }
    }

    public void add(DoubleDireNode node){
        if(null == root){
            this.root = node;
            root.next = root;
            root.pre = root;
            this.count = 1;
        }else{
            DoubleDireNode node1 = root;
            while(node1.next!=root){
                node1 = node1.next;
            }
            node1.next = node;
            node.next = root;
            node.pre = node1;
            root.pre = node;
            count++;
        }
    }

    public Object removeRoot(){
        Object data =root.data;
        DoubleDireNode newRoot = root.next;
        root.next.pre = root.pre;
        root.pre.next = root.next;
        root.pre = null;
        root.next = null;
        root = newRoot;
        count--;
        return data;
    }

    public void print(){
        System.out.println("count="+count);
        DoubleDireNode node = root;
        do{
            System.out.println(node.data);
            node = node.next;
        }while(node!=root);
    }
}

class DoubleDireNode{

    DoubleDireNode pre;
    DoubleDireNode next;
    Object data;

    public DoubleDireNode(Object data,DoubleDireNode next){
        this.data = data;
        this.next = next;
    }

    public DoubleDireNode(Object data){
        this.data = data;
        this.next = null;
    }

    //这个add是加在后面，LinkNode的add是加在末尾。DoubleDireNode的add在不知道root的情况下无法加到末尾
    public void add(DoubleDireNode node){
        node.next = this.next;
        this.next = node;
        node.pre = this;
    }

    public void removeAfter() throws Exception{
        if(this.next!=this){
            DoubleDireNode node = this.next;
            this.next.pre = null;
            this.next.next.pre = this;
            this.next=this.next.next;
            node.next = null;

        }else{
            throw new Exception("当前链表中只有一个节点");
        }
    }

}
