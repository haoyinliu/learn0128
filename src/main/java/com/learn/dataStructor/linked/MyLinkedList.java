package com.learn.dataStructor.linked;

/**
 * @Author: Yinliu Hao
 * @Description:
 * @Date: Created in 2022/2/9 22:21
 * @Modify By:
 */
//Node节点和链条类上都定义方法
public class MyLinkedList {

    protected LinkNode root;
    protected int count;

    public MyLinkedList(){

    }

    public MyLinkedList(LinkNode root){
        this.root = root;
        this.count = 1;
    }


    public MyLinkedList(int count){
        for(int i=1;i<=count;i++){
            add(new LinkNode(i));
        }
    }

    public void add(LinkNode node){
        if(null == root){
            this.root = node;
            this.count = 1;
        }else{
            this.root.add(node);
            count++;
        }
    }

    public void print(){
        System.out.println("count="+count);
        if(null!=root){
            root.print();
        }
    }


    public static void main(String[] args) {

        MyLinkedList list = new MyLinkedList(10);
        list.print();

    }

    public static void fun1(){
        LinkNode node = new LinkNode("1", null);
        node.add(new LinkNode("2"));
        node.add(new LinkNode("3"));
        node.add(new LinkNode("4"));
        node.add(new LinkNode("5"));
        node.print();
        System.out.println("-----------------");
        node.removeAfter();
        node.print();
        System.out.println("-----------------");
        node.insertAfter(new LinkNode("7"));
        node.print();
    }

    public static void fun2(){

    }

    public static MyLinkedList init(){

        return null;
    }

}
class LinkNode{
    Object data;
    LinkNode next;

    public LinkNode(Object data,LinkNode next){
        this.data = data;
        this.next = next;
    }

    public LinkNode(Object data){
        this.data = data;
        this.next = null;
    }

    public void add(LinkNode node){
        if(null==this.next){
            this.next = node;
        }else{
            this.next.add(node);
        }
    }

    public void removeAfter(){
        if(null!=this.next){
            if(null!=this.next.next){
                this.next = this.next.next;
            }else{
                this.next  = null;
            }
        }
    }

    public void insertAfter(LinkNode node){
        node.next = this.next;
        this.next = node;
    }

    public void print(){
        System.out.println(this.data);
        if(null!=this.next){
            this.next.print();
        }

    }

}