package com.learn.dataStructor.linked;

import java.util.Objects;

/**
 * @Author: Yinliu Hao
 * @Description:
 * @Date: Created in 2022/2/9 20:42
 * @Modify By:
 */
//自己写的这个链表，把增删查方法定义在了链条类上.在MyLinkedList类里尝试在Node节点和链条类上都定义方法
public class MyLinkedNode {

    private MyNode root;
    private MyNode foot;

    public static void main(String[] args) {

        MyLinkedNode myLinkedNode = init();

        //fun2(myLinkedNode);

    }

    public static MyLinkedNode init(){
        MyLinkedNode myLinkedNode = new MyLinkedNode();
        MyNode node1 = new MyNode("1");
        MyNode node2 = new MyNode("2");
        MyNode node3 = new MyNode("3");
        MyNode node4 = new MyNode("4");
        MyNode node5 = new MyNode("5");
        myLinkedNode.add(node1);
        myLinkedNode.add(node2);
        myLinkedNode.add(node3);
        myLinkedNode.add(node4);
        myLinkedNode.add(node5);
        node1.print();
        return myLinkedNode;
    }

    public static void fun1(MyLinkedNode myLinkedNode){
        MyNode node33 = new MyNode("3");
        MyNode res = myLinkedNode.getByNode(node33);
        System.out.println(res.data);
    }

    public static void fun2(MyLinkedNode myLinkedNode){
        MyNode node33 = new MyNode("3");
        myLinkedNode.delete(node33);
        myLinkedNode.print();
    }

    public MyLinkedNode(){

    }

    public MyLinkedNode(MyNode root){
        this.root = root;
        this.foot = root;
    }

    public void add(MyNode node){
        if(null == root){
            this.root = node;
            this.foot = node;
        }else{
            this.foot.next = node;
            this.foot = node;
        }
    }

    public MyNode get(String data){
        MyNode node = root;
        while(null!=node){
            if(data.equals((String)node.data)){
                return node;
            }else{
                node = node.next;
            }
        }
        return null;
    }

    public MyNode getByNode(MyNode myNode){
        MyNode node = root;
        while(null!=node){
            if(((String)myNode.data).equals((String)node.data)){
                return node;
            }else{
                node = node.next;
            }
        }
        return null;
    }

    public void delete(MyNode myNode){
        MyNode node = root;
        while(null!=node){
            if(null!=node.next && ((String)node.next.data).equals((String)(myNode.data))){
                node.next = node.next.next;
                break;
            }
            node = node.next;
        }
    }

    public void print(){
        /*MyNode node = root;
        while(null != node){
            System.out.println(node.data);
            node = node.next;
        }*/
        if(null!=root){
            root.print();
        }
    }

}

class MyNode{

    protected Object data;
    protected MyNode next;

    public MyNode(Object data){
        this.data = data;
        this.next = null;
    }

    public MyNode(Object data, MyNode next) {
        this.data = data;
        this.next = next;
    }

    public void print(){
        System.out.println(data);
        if(null !=this.next){
            this.next.print();
        }
    }

    //重写equals

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MyNode myNode = (MyNode) o;
        return Objects.equals(data, myNode.data);
    }

    @Override
    public int hashCode() {
        return Objects.hash(data, next);
    }
}

