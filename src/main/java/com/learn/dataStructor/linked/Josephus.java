package com.learn.dataStructor.linked;

public class Josephus {


    public static void main(String[] args) throws Exception{
       int result =  Josephus.input(10, 3);
       System.out.println(result);
    }

    //考虑根节点也会被删掉
    public static int input(int m,int n) throws Exception{

        JosephusLink link = new JosephusLink(m);
        JosephusNode node= link.root;
        while(link.count>1){
            JosephusNode result = link.getAfterSeveral(node, n-1);
            link.removeAfter(result);
            node = result.next;
        }
        return link.root.index;
    }
}
